﻿using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using CoreGraphics;
using VideoSubscriberAccount;

namespace AccessEnabler_iOS
{
    // @protocol JSMVPDExport
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface JSMVPDExport
    {
        // @required @property (nonatomic, strong) NSString * ID;
        [Abstract]
        [Export("ID", ArgumentSemantic.Strong)]
        string ID { get; set; }

        // @required @property (nonatomic, strong) NSString * displayName;
        [Abstract]
        [Export("displayName", ArgumentSemantic.Strong)]
        string DisplayName { get; set; }

        // @required @property (nonatomic, strong) NSString * logoURL;
        [Abstract]
        [Export("logoURL", ArgumentSemantic.Strong)]
        string LogoURL { get; set; }

        // @required @property (nonatomic, strong) NSString * spUrl;
        [Abstract]
        [Export("spUrl", ArgumentSemantic.Strong)]
        string SpUrl { get; set; }

        // @required @property (assign, nonatomic) BOOL tempPass;
        [Abstract]
        [Export("tempPass")]
        bool TempPass { get; set; }
    }

    // @interface MVPD : NSObject <JSMVPDExport>
    [BaseType(typeof(NSObject))]
    [Protocol]
    interface MVPD : JSMVPDExport
    {
        // @property (nonatomic, strong) NSString * ID;
        [Export("ID", ArgumentSemantic.Strong)]
        string ID { get; set; }

        // @property (nonatomic, strong) NSString * displayName;
        [Export("displayName", ArgumentSemantic.Strong)]
        string DisplayName { get; set; }

        // @property (nonatomic, strong) NSString * logoURL;
        [Export("logoURL", ArgumentSemantic.Strong)]
        string LogoURL { get; set; }

        // @property (nonatomic, strong) NSString * spUrl;
        [Export("spUrl", ArgumentSemantic.Strong)]
        string SpUrl { get; set; }

        // @property (assign, nonatomic) BOOL hasAuthNPerRequestor;
        [Export("hasAuthNPerRequestor")]
        bool HasAuthNPerRequestor { get; set; }

        // @property (assign, nonatomic) BOOL passiveAuthnEnabled;
        [Export("passiveAuthnEnabled")]
        bool PassiveAuthnEnabled { get; set; }

        // @property (assign, nonatomic) BOOL tempPass;
        [Export("tempPass")]
        bool TempPass { get; set; }

        // @property (assign, nonatomic) BOOL enablePlatformServices;
        [Export("enablePlatformServices")]
        bool EnablePlatformServices { get; set; }

        // @property (assign, nonatomic) BOOL enforcePlatformPermissions;
        [Export("enforcePlatformPermissions")]
        bool EnforcePlatformPermissions { get; set; }

        // @property (assign, nonatomic) BOOL displayInPlatformPicker;
        [Export("displayInPlatformPicker")]
        bool DisplayInPlatformPicker { get; set; }

        // @property (nonatomic, strong) NSString * boardingStatus;
        [Export("boardingStatus", ArgumentSemantic.Strong)]
        string BoardingStatus { get; set; }

        // @property (nonatomic, strong) NSString * platformMappingId;
        [Export("platformMappingId", ArgumentSemantic.Strong)]
        string PlatformMappingId { get; set; }

        // @property (nonatomic, strong) NSMutableArray * requiredMetadataFields;
        [Export("requiredMetadataFields", ArgumentSemantic.Strong)]
        NSMutableArray RequiredMetadataFields { get; set; }

        // -(NSString *)toString;
        //[Export("toString")]
        //[Verify(MethodToProperty)]
        //string ToString { get; }

        // -(NSString *)description;
        //[Export("description")]
        //[Verify(MethodToProperty)]
        //string Description { get; }
    }

    // @protocol EntitlementDelegate <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface EntitlementDelegate
    {
        // @required -(void)setRequestorComplete:(int)status;
        [Abstract]
        [Export("setRequestorComplete:")]
        void SetRequestorComplete(int status);

        // @required -(void)setAuthenticationStatus:(int)status errorCode:(NSString *)code;
        [Abstract]
        [Export("setAuthenticationStatus:errorCode:")]
        void SetAuthenticationStatus(int status, string code);

        // @required -(void)setToken:(NSString *)token forResource:(NSString *)resource;
        [Abstract]
        [Export("setToken:forResource:")]
        void SetToken(string token, string resource);

        // @required -(void)preauthorizedResources:(NSArray *)resources;
        [Abstract]
        [Export("preauthorizedResources:")]
        //[Verify(StronglyTypedNSArray)]
        void PreauthorizedResources(NSObject[] resources);

        // @required -(void)tokenRequestFailed:(NSString *)resource errorCode:(NSString *)code errorDescription:(NSString *)description;
        [Abstract]
        [Export("tokenRequestFailed:errorCode:errorDescription:")]
        void TokenRequestFailed(string resource, string code, string description);

        // @required -(void)selectedProvider:(MVPD *)mvpd;
        [Abstract]
        [Export("selectedProvider:")]
        void SelectedProvider(MVPD mvpd);

        // @required -(void)displayProviderDialog:(NSArray *)mvpds;
        [Abstract]
        [Export("displayProviderDialog:")]
        //[Verify(StronglyTypedNSArray)]
        void DisplayProviderDialog(NSObject[] mvpds);

        // @required -(void)sendTrackingData:(NSArray *)data forEventType:(int)event;
        [Abstract]
        [Export("sendTrackingData:forEventType:")]
        //[Verify(StronglyTypedNSArray)]
        void SendTrackingData(NSObject[] data, int @event);

        // @required -(void)setMetadataStatus:(id)metadata encrypted:(BOOL)encrypted forKey:(int)key andArguments:(NSDictionary *)arguments;
        [Abstract]
        [Export("setMetadataStatus:encrypted:forKey:andArguments:")]
        void SetMetadataStatus(NSObject metadata, bool encrypted, int key, NSDictionary arguments);

        // @required -(void)navigateToUrl:(NSString *)url;
        [Abstract]
        [Export("navigateToUrl:")]
        void NavigateToUrl(string url);

        // @required -(void)presentTvProviderDialog:(id)viewController;
        [Abstract]
        [Export("presentTvProviderDialog:")]
        void PresentTvProviderDialog(NSObject viewController);

        // @required -(void)dismissTvProviderDialog:(id)viewController;
        [Abstract]
        [Export("dismissTvProviderDialog:")]
        void DismissTvProviderDialog(NSObject viewController);
    }

    // @protocol EntitlementStatus <NSObject>
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface EntitlementStatus
    {
        // @required -(void)status:(NSDictionary *)statusDictionary;
        [Abstract]
        [Export("status:")]
        void Status(NSDictionary statusDictionary);
    }

    // @protocol JSAEExport
    [Protocol, Model]
    [BaseType(typeof(NSObject))]
    interface JSAEExport
    {
        // @required -(void)checkAuthentication;
        [Abstract]
        [Export("checkAuthentication")]
        void CheckAuthentication();

        // @required -(void)getAuthentication;
        [Abstract]
        [Export("getAuthentication")]
        void GetAuthentication();

        // @required -(void)getAuthenticationToken;
        [Abstract]
        [Export("getAuthenticationToken")]
        void GetAuthenticationToken();

        // @required -(void)getSelectedProvider;
        [Abstract]
        [Export("getSelectedProvider")]
        void GetSelectedProvider();

        // @required -(void)logout;
        [Abstract]
        [Export("logout")]
        void Logout();
    }

    // @interface ObjC : NSObject
    [BaseType(typeof(NSObject))]
    [Protocol]
    interface ObjC
    {
        // +(BOOL)catchException:(void (^)())tryBlock error:(NSError **)error;
        [Static]
        [Export("catchException:error:")]
        bool CatchException(Action tryBlock, out NSError error);
    }

    // @interface AccessEnabler : NSObject <JSAEExport>
    [BaseType(typeof(NSObject))]
    [Protocol]
    interface AccessEnabler : JSAEExport
    {
        [Wrap("WeakDelegate")]
        [NullAllowed]
        EntitlementDelegate Delegate { get; set; }

        // @property (weak) id<EntitlementDelegate> _Nullable delegate;
        [NullAllowed, Export("delegate", ArgumentSemantic.Weak)]
        NSObject WeakDelegate { get; set; }

        [Wrap("WeakStatusDelegate")]
        [NullAllowed]
        EntitlementStatus StatusDelegate { get; set; }

        // @property (weak) id<EntitlementStatus> _Nullable statusDelegate;
        [NullAllowed, Export("statusDelegate", ArgumentSemantic.Weak)]
        NSObject WeakStatusDelegate { get; set; }

        // -(void)setOptions:(NSDictionary * _Nonnull)options;
        [Export("setOptions:")]
        void SetOptions(NSDictionary options);

        // -(void)setRequestor:(NSString * _Nonnull)requestorID setSignedRequestorId:(NSString * _Nonnull)signedRequestorId;
        [Export("setRequestor:setSignedRequestorId:")]
        void SetRequestor(string requestorID, string signedRequestorId);

        // -(void)setRequestor:(NSString * _Nonnull)requestorID setSignedRequestorId:(NSString * _Nonnull)signedRequestorId serviceProviders:(NSArray * _Nullable)urls;
        [Export("setRequestor:setSignedRequestorId:serviceProviders:")]
        //[Verify(StronglyTypedNSArray)]
        void SetRequestor(string requestorID, string signedRequestorId, [NullAllowed] string[] urls);

        // -(void)checkAuthentication;
        [Export("checkAuthentication")]
        void CheckAuthentication();

        // -(void)getAuthentication;
        [Export("getAuthentication")]
        void GetAuthentication();

        // -(void)getAuthentication:(NSDictionary * _Nullable)filter;
        [Export("getAuthentication:")]
        void GetAuthentication([NullAllowed] NSDictionary filter);

        // -(void)getAuthentication:(BOOL)forceAuthn withData:(NSDictionary * _Nullable)data;
        [Export("getAuthentication:withData:")]
        void GetAuthentication(bool forceAuthn, [NullAllowed] NSDictionary data);

        // -(void)getAuthentication:(BOOL)forceAuthn withData:(NSDictionary * _Nullable)data andFilter:(NSDictionary * _Nullable)filter;
        [Export("getAuthentication:withData:andFilter:")]
        void GetAuthentication(bool forceAuthn, [NullAllowed] NSDictionary data, [NullAllowed] NSDictionary filter);

        // -(void)getAuthenticationToken;
        [Export("getAuthenticationToken")]
        void GetAuthenticationToken();

        // -(void)checkAuthorization:(NSString * _Nullable)resource;
        [Export("checkAuthorization:")]
        void CheckAuthorization([NullAllowed] string resource);

        // -(void)checkAuthorization:(NSString * _Nullable)resource withData:(NSDictionary * _Nullable)data;
        [Export("checkAuthorization:withData:")]
        void CheckAuthorization([NullAllowed] string resource, [NullAllowed] NSDictionary data);

        // -(void)getAuthorization:(NSString * _Nullable)resource;
        [Export("getAuthorization:")]
        void GetAuthorization([NullAllowed] string resource);

        // -(void)getAuthorization:(NSString * _Nullable)resource withData:(NSDictionary * _Nullable)data;
        [Export("getAuthorization:withData:")]
        void GetAuthorization([NullAllowed] string resource, [NullAllowed] NSDictionary data);

        // -(void)checkPreauthorizedResources:(NSArray * _Nullable)resources;
        [Export("checkPreauthorizedResources:")]
        //[Verify(StronglyTypedNSArray)]
        void CheckPreauthorizedResources([NullAllowed] NSObject[] resources);

        // -(void)setSelectedProvider:(NSString * _Nullable)mvpdID;
        [Export("setSelectedProvider:")]
        void SetSelectedProvider([NullAllowed] string mvpdID);

        // -(void)getSelectedProvider;
        [Export("getSelectedProvider")]
        void GetSelectedProvider();

        // -(void)getMetadata:(NSDictionary * _Nullable)key;
        [Export("getMetadata:")]
        void GetMetadata([NullAllowed] NSDictionary key);

        // -(void)logout;
        [Export("logout")]
        void Logout();

        // -(void)dispatchStatus:(NSDictionary * _Nullable)statusDict;
        [Export("dispatchStatus:")]
        void DispatchStatus([NullAllowed] NSDictionary statusDict);
    }
}
