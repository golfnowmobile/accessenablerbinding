﻿using System;

namespace AccessEnabler_iOS
{
	public struct Structs {
        public const string SP_AUTH_PRODUCTION = "sp.auth.adobe.com/adobe-services";
        public const string SP_AUTH_STAGING    = "sp.auth-staging.adobe.com/adobe-services";

        //public const string DEFAULT_SP_URL SP_AUTH_PRODUCTION

        //#pragma mark - AdobePass web-service paths
        public const string SP_URL_DOMAIN_NAME                     = "adobe.com";
        public const string SP_URL_PATH_SET_REQUESTOR              = "/config/";
        public const string SP_URL_PATH_GET_AUTHENTICATION         = "/authenticate/saml";
        public const string SP_URL_PATH_GET_AUTHENTICATION_TOKEN   = "/sessionDevice";
        public const string SP_URL_PATH_GET_AUTHORIZATION_TOKEN   =  "/authorizeDevice";
        public const string SP_URL_PATH_GET_SHORT_MEDIA_TOKEN      = "/deviceShortAuthorize";
        public const string SP_URL_PATH_LOGOUT                     = "/logout";
        public const string SP_URL_PATH_GET_DEVICEID_METADATA      = "/getMetadataDevice";
        public const string SP_URL_PATH_GET_ENCRYPTED_USER_METADATA= "/usermetadata";
        public const string SP_URL_IDENTIFIER_PASSIVE_AUTHN        ="/completePassiveAuthentication";

        public const string SP_URL_PATH_CHECK_PREAUTHZ_RESOURCES   = "/preauthorize";
        public const string ADOBEPASS_REDIRECT_URL                 ="http://adobepass.ios.app/";

        public const string CLIENT_TYPE_IOS="iOS";
        public const string CLIENT_TYPE_AIR="AIR_iOS";

        //#pragma mark - AccessEnabler status codes
        public const int ACCESS_ENABLER_STATUS_ERROR =0;
        public const int ACCESS_ENABLER_STATUS_SUCCESS  =1;

        //#pragma mark - AccessEnabler error codes
        public const string USER_AUTHENTICATED              ="";
        public const string USER_NOT_AUTHENTICATED_ERROR   = "User Not Authenticated Error";
        public const string USER_NOT_AUTHORIZED_ERROR      = "User Not Authorized Error";
        public const string PROVIDER_NOT_SELECTED_ERROR    ="Provider Not Selected Error";
        public const string GENERIC_AUTHENTICATION_ERROR   ="Generic Authentication Error";
        public const string GENERIC_AUTHORIZATION_ERROR    = "Generic Authorization Error";
        public const string SERVER_API_TOO_OLD             = "API Version too old. Please update your application.";

        //#pragma mark - getMetadata operation codes
        public const int METADATA_AUTHENTICATION     =0;
        public const int METADATA_AUTHORIZATION     = 1;
        public const int METADATA_DEVICE_ID         = 2;
        public const int METADATA_USER_META         = 3;
        public const int METADATA_KEY_INVALID       = 255;
        public const string METADATA_OPCODE_KEY        = "opCode";
        public const string METADATA_RESOURCE_ID_KEY   = "resource_id";
        public const string METADATA_USER_META_KEY     = "user_metadata_name";

        //#pragma mark - sendTrackingData event types
        public const int TRACKING_AUTHENTICATION       =  0;
        public const int TRACKING_AUTHORIZATION        =  1;
        public const int TRACKING_MVPD_SELECTION       =  2;

        //#pragma mark - sendTrackingData return values
        public const string TRACKING_NONE = "None";
        public const string TRACKING_YES ="YES";
        public const string TRACKING_NO  = "NO";
    }
 //   static class CFunctions
 //   {
 //       // extern int JSExportAs ();
 //       [DllImport ("__Internal")]
 //       [Verify (PlatformInvoke)]
 //       static extern int JSExportAs ();
	//}
}